public virtual class Vehicle {
    public void model(){
        System.debug('Model method of Vehicle class');
    }
    
    public virtual void speed(){
        System.debug('Speed of vehicla depends on the type');
    }

}