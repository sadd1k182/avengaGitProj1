public class StaticExample {
    public static void method1(){
        System.debug('I am a static method');
    }
    
    public void method2(){
        System.debug('I am a non-static method');
    }

}