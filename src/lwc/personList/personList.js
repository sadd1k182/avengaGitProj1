import { LightningElement } from 'lwc';

export default class PersonList extends LightningElement {
    persons = [
        {
            id: 1,
            name: 'Richard Hendricks'
        },
        {
            id: 2,
            name: 'Monica'
        },
        {
            id: 3,
            name: 'Terrence Ross'
        },
        {
            id:4,
            name: 'Baby Shaqq'
        },
        {
            id: 5,
            name: 'Japukar Haparbesh'
        }
    ]
}